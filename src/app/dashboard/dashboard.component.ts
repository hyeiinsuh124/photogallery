import { Component, OnInit } from '@angular/core';
import { HotosService } from   '../services/hotos.service';
import { Hoto } from 'src/app/class/hoto';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(public photo_service: HotosService) { }

  photos: Hoto[];
  ngOnInit() {
    this.getPhotos();
  }

  async getPhotos(){
    await this.photo_service.getPhotos().subscribe(photos => {
      this.photos = photos;
      console.log('Photos: ', this.photos);
    });
  }

}
