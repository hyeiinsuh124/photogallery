import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsmainMenuComponent } from './componentsmain-menu.component';

describe('ComponentsmainMenuComponent', () => {
  let component: ComponentsmainMenuComponent;
  let fixture: ComponentFixture<ComponentsmainMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsmainMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsmainMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
