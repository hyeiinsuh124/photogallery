import { TestBed } from '@angular/core/testing';

import { HotosService } from './hotos.service';

describe('HotosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HotosService = TestBed.get(HotosService);
    expect(service).toBeTruthy();
  });
});
