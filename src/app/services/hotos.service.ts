import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Hoto } from '../class/hoto';
import { Observable, throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HotosService {
  //protected: usable for child
  constructor(private http: HttpClient) { }
  //Create a constant indicating source URL
  photoUrl = "https://picsum.photos/v2/list";
  
  getPhotos():Observable<Hoto[]>{
    return this.http.get<Hoto[]>(this.photoUrl)
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse){
    if(error.error instanceof ErrorEvent){
      console.error('An error ocurred: ', error.error.message);
    }else{
      console.error('Status: ', error.status);
      console.error('Body was: ', error.error);
    }
    return throwError('Something bad happened, Please try again later!');
  }
}
